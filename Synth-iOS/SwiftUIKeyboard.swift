//
//  SwiftUIKeyboard.swift
//  Synth-iOS
//
//  This file consists the keyboard to tap and play the audio.
//

import Foundation
import SwiftUI
import Keyboard
import Tonic
import AudioKit


//This is the Keyboard UI implementation for the analog synthesizer.
struct SwiftUIKeyboard: View {
    var noteOn: (Pitch, CGPoint) -> Void = { _, _ in }
    var noteOff: (Pitch) -> Void

    let firstNote = 33  // MIDI note number for the lowest pitch
    let lastNote = 84   // MIDI note number for the highest pitch
    let pitchRange: ClosedRange<Pitch>

    init(noteOn: @escaping (Pitch, CGPoint) -> Void, noteOff: @escaping (Pitch) -> Void) {
        self.noteOn = noteOn
        self.noteOff = noteOff
        self.pitchRange = Pitch(intValue: firstNote) ... Pitch(intValue: lastNote)
    }

    var body: some View {
        Keyboard(layout: .piano(pitchRange: pitchRange), noteOn: noteOn, noteOff: noteOff) { pitch, isActivated in
            KeyboardKey(pitch: pitch,
                        isActivated: isActivated,
                        pressedColor: Color.red,
                        flatTop: true)
                .gesture(DragGesture(minimumDistance: 0)
                        .onChanged { value in
                            let location = value.location
                            noteOn(pitch, location)
                        }
                        .onEnded { value in
                            noteOff(pitch)
                        })
        }
        .cornerRadius(5)
    }
}
