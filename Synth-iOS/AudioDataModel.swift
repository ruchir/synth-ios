//
//  AudioDataModel.swift
//  Synth-iOS
//
//  This file also consists the model for the data. We have the knobs created for these to change and play the audio.
//

import Foundation
import Keyboard
import Tonic
import AudioKit

struct MorphingOscillatorData {
    var frequency: AUValue = 440
    var amplitude: AUValue = 0.2
    var resonance: AUValue = 0.5
    var attack: AUValue = 0.0
    var decay: AUValue = 1.0
}
