//
//  Synth_iOSApp.swift
//  Synth-iOS
//
//  This is where the program execution begins.

import SwiftUI

@main
struct Synth_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            SynthView()
        }
    }
}
