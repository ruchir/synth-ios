//
//  ContentView.swift
//  Synth-iOS
//
//  This file contains the core implementation of the analog synthesizer. This file consist the user interface implementation for
//  the application (knobs, sliders, text, etc.). The design patter used here is MVVM (Model-View-View Model), where the SynthClass
//  is the view model, SynthView is the view and AudioDataModel is the model.
//

import SwiftUI
import AudioKit
import Keyboard
import SoundpipeAudioKit
import Controls


//This is the view model for the application, which consists all the fucntion implementations, defines variables
class SynthClass: ObservableObject {
    let engine = AudioEngine()

    @Published var env: AmplitudeEnvelope

    @Published var cutoff: AUValue = 15000 {
        didSet { filter?.cutoffFrequency = cutoff }
    }

    @Published var resonance: AUValue = 0.5 {
        didSet { filter?.resonance = resonance }
    }                                                                   //Initializing all the variables.

    @Published var amplitude: AUValue = 0.2 {
        didSet { env.sustainLevel = amplitude }
    }

    @Published var attack: AUValue = 0.0 {
        didSet { env.attackDuration = attack }
    }

    @Published var decay: AUValue = 1.0 {
        didSet { env.decayDuration = decay }
    }
    
    var osc: Oscillator
    var filter: MoogLadder?
    
    var noiseMixer: Mixer
    var noiseVolume: AUValue = 0.0 {
        didSet { noiseMixer.volume = noiseVolume }
    }

    
    //Default sine wave selected.
    @Published var selectedOption: String = "Sine" {
        didSet {
            updateWaveform()
        }
    }

    //This function allows the user to view different audio. So, we first stop the existing osciallator, change the waveform, create
    // a new environment and play. By defualt, we have a sine wave selected.
    func updateWaveform() {
        osc.stop()

        switch selectedOption {
            case "Square":
                osc = Oscillator(waveform: Table(.square))
                filter = MoogLadder(Mixer(osc), cutoffFrequency: cutoff, resonance: resonance)
            case "Triangle":
                osc = Oscillator(waveform: Table(.triangle))
                filter = MoogLadder(Mixer(osc), cutoffFrequency: cutoff, resonance: resonance)
            case "Saw":
                osc = Oscillator(waveform: Table(.sawtooth))
                filter = MoogLadder(Mixer(osc), cutoffFrequency: cutoff, resonance: resonance)
            default:
                osc = Oscillator(waveform: Table(.sine))
                filter = MoogLadder(Mixer(osc), cutoffFrequency: cutoff, resonance: resonance)
        }

        osc.start()
        osc.$frequency.ramp(to: data.frequency, duration: 0.1)

        let newEnv = AmplitudeEnvelope(filter ?? osc, attackDuration: attack, decayDuration: decay, sustainLevel: amplitude, releaseDuration: 0.25)
        self.env = newEnv
        engine.output = newEnv
    }

    var data = MorphingOscillatorData() {
        didSet {
            osc.start()
            osc.$frequency.ramp(to: data.frequency, duration: 0.1)
        }
    }

    //This the initializer for the application upon execution of the class. Variables are initialized to their default values.
    init() {
        osc = Oscillator(waveform: Table(.sine))
        filter = MoogLadder(Mixer(osc), cutoffFrequency: 15000, resonance: 0.5)
        env = AmplitudeEnvelope(filter ?? osc, attackDuration: 0.0, decayDuration: 1.0, sustainLevel: 0.0, releaseDuration: 0.25)
        
        let noise = WhiteNoise()
        noiseMixer = Mixer(noise)
        noiseMixer.volume = noiseVolume
        engine.output = Mixer(env, noiseMixer)

        try? engine.start()
    }
    

    func noteOn(pitch: Pitch, point: CGPoint) {
        data.frequency = AUValue(pitch.midiNoteNumber).midiNoteToFrequency()
        
        //amplitude needs to be greater than 0 for the audio to be heard.
        if amplitude > 0 {
            env.closeGate()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                self.env.openGate()
            }
        }
    }

    func noteOff(pitch: Pitch) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.env.closeGate()
        }
    }
}


//This is UI for the analog synthesizer and it communicates with the SynthClass to update the audio accordingly
struct SynthView: View {
    @ObservedObject var conductor = SynthClass()

    var body: some View {
        VStack {
            HStack {
                AudioOptionsView(selectedOption: $conductor.selectedOption)
            
                VStack {
                    
                    Text("Filter\n\(Int(conductor.cutoff))")
                        .multilineTextAlignment(.center)
                        .padding(.top, 8)
                    
                    SmallKnob(value: $conductor.cutoff, range: 12.0 ... 15000.0)
                        .frame(width: 110, height: 80)
                        .padding(.bottom, 10)
                }
                
                VStack {
                    
                    Text("Resonance\n\(conductor.resonance)")
                        .multilineTextAlignment(.center)
                        .padding(.top, 8)
                    
                    SmallKnob(value: $conductor.resonance, range: 0.0 ... 1.0)
                        .frame(width: 110, height: 80)
                        .padding(.bottom, 10)
                }

                VStack {
                    Text("Amplitude\n\(conductor.amplitude)")
                        .multilineTextAlignment(.center)
                        .padding(.top, 10)

                    SmallKnob(value: $conductor.amplitude, range: 0.0 ... 1.0)
                        .frame(width: 110, height: 80)
                        .padding(.bottom, 10)
                }

                VStack {
                    Text("Noise Volume\n\(conductor.noiseVolume)")
                        .multilineTextAlignment(.center)
                        .padding(.top, 10)
                    
                    SmallKnob(value: $conductor.noiseVolume, range: 0.0 ... 1.0)
                        .frame(width: 110, height: 80)
                        .padding(.bottom, 10)
                }
                
                VStack {
                    Text("Attack\n\(conductor.attack)")
                        .multilineTextAlignment(.center)
                        .padding(.top, 10)

                    Slider(value: $conductor.attack, in: 0.0 ... 2.0, step: 0.01)
                        .frame(width: 120, height: 40)
                        .padding(.bottom, 10)
                }
                
                VStack {
                    Text("Decay\n\(conductor.decay)")
                        .multilineTextAlignment(.center)
                        .padding(.top, 10)

                    Slider(value: $conductor.decay, in: 0.0 ... 2.0, step: 0.01)
                        .frame(width: 120, height: 40)
                        .padding(.bottom, 10)
                }
            }

            SwiftUIKeyboard(noteOn: conductor.noteOn(pitch:point:), noteOff: conductor.noteOff)
                .frame(maxHeight: 300)
            
        }
        .background(Color(.systemGray4))
        .onDisappear() {
            conductor.engine.stop()
        }
    }
}
