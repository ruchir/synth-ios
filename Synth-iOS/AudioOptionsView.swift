//
//  AudioOptionsView.swift
//  Synth-iOS
//
//  This file consists the user interface the audio options for the waveform to play audio.
//

import Foundation
import SwiftUI

//This view is called in the Content View file which is the home of the entire view.
struct AudioOptionsView: View {
    
    @Binding var selectedOption: String
    let options = ["Square", "Triangle", "Sine", "Saw"]             //Users have 4 options to select from.

    var body: some View {
        VStack {
            Picker("Waveform", selection: $selectedOption) {        //Options are available via a dropdown menu.
                ForEach(options, id: \.self) { option in
                    Text(option)
                }
            }
            .pickerStyle(MenuPickerStyle())

        }
    }
}
