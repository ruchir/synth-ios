# Synth iOS
 - Ruchir Elukurthy

# Video presentation
https://pdx.zoom.us/rec/share/N8C2xnD_ygLbk6its5rzrIB9EqCaGkZXn7Vik1XsU0MP-0OsAJ_BCjYxXis9t0TZ.iTzJh-flbQdX58A-?startTime=1686789622000

## Description

This is an iOS app of an analog synthesizer which consists of a user interface with knobs and a keyboard that allows them to generate different sounds. The users will have an option to select different types of oscillators namely, sine, square, sawtooth and triangle wave. Then, there are knobs that allow the user to alter the frequency until a given cutoff frequency, and the same goes for amplitude, resonance and noise. This app is created with the intention to have a graphical user interface for an analog synthesizer replacing the need to have an actual physical device to run it and also making it convenient to play it on a phone or iPad. A lot of options that are available are paid on the App Store.

## How to use this project?

Download XCode from the Mac App Store on the latest version available.
Clone the application into your local machine.
You’ll see a file ending in .xcodeproj (Synth-iOS.xcodeproj). Open with XCode.
Once you are in XCode, you can click the run button at the top left of the screen
(Note: This may take a while to load and run depending on the type of machine you are on. I’ve tested it on an Intel Macbook and it takes about 90-120 seconds to load and run on the first try.)
Once the code builds and runs the IDE will shift to the simulator that will show you the GUI. Click on the top right corner button to rotate the screen horizontally for a better view experience.
You can change the knobs for the mentioned functionality and select the waveform and play around with the app.
If you wish to run this application on your phone, connect your iPhone running on at least iOS 16 to your Macbook and select your device from the list of devices with the top bar of XCode (You’ll see this next to the name of the app on XCode).
You should be able to play on your phone once this is done, unless you have some privacy/security concerns or layers setup on your phone.


## Example illustration:

Following the steps to run the project. You can change the knobs for the mentioned functionality and select the waveform and play around with the app. By default you have a sine wave selected, but select whichever one you like and change the knobs to anything you’d like for the other functionality mentioned previously.

## What worked? What didn't:
The application works well for the features implemented, but everything looks congested for now. I had planned to implement ADSR envelope sliders too, but they seem to make the app too cluttered, and I need to figure out which functionality to keep or remove. Space is the issue when building such an app on the iPhone. If this app was a full scale application with the intention of deployment, I would work with a UI/UX designer to make the app more user friendly. Also, I’d spend more time on MIDI mapping functionality which I may not be able to do in my case. I was also trying to have a waveform creator, which seems too complex for the scope of this project.

## Authors and acknowledgment
Thanks for open source librarries from AudioKit (https://github.com/AudioKit/AudioKit) and Nick Culbertson https://github.com/NickCulbertson/100-Lines-of-Code-AudioKit-Examples/tree/main/100LinesOfCode/100LinesOfCode for making this project possible
